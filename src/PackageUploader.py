import os


class PackageUploader:
    def upload_beta_package(self):
        # login
        # go to URL/0A2?setupid=Package
        # click on Package Name hyperlink
        # click on upload button
        # set version name
        # click upload button
        # Check Queued Status (div.message)
        # Check Test Status
        # Check final Result

        pass

    def upload_release_package(self):
        # login
        # go to URL/0A2?setupid=Package
        # click on Package Name hyperlink
        # click on upload button
        # set version name
        # set Release Type to Managed - Released
        # click upload button
        # Check Queued Status (div.message)
        # Check Test Status
        # Check final Result

        pass

    def get_feature_username(self):
        return os.environ['SF_USERNAME_PACKAGING']

    def get_feature_password(self):
        return os.environ['SF_PASSWORD_PACKAGING']
