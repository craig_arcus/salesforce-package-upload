from unittest import TestCase
from test.support import EnvironmentVarGuard

from src.PackageUploader import PackageUploader


class TestPackageUploader(TestCase):
    def setUp(self):
        self.uploader = PackageUploader()
        self.setUpEnvironmentVariables()

    def setUpEnvironmentVariables(self):
        self.environment = EnvironmentVarGuard()
        self.environment.set('SF_USERNAME_PACKAGING', 'goat@herder.com')
        self.environment.set('SF_PASSWORD_PACKAGING', 'strangerThings')

    def test_get_packaging_username_returns_correct_value(self):
        username = self.uploader.get_feature_username()

        self.assertEquals('goat@herder.com', username)

    def test_get_packaging_password_returns_correct_password(self):
        password = self.uploader.get_feature_password()

        self.assertEqual('strangerThings', password)
